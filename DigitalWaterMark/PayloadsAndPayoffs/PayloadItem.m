//
//  PayloadItem.m
//  Digimarc Mobile SDK: DMSDemo
//
//  Created by localTstewart on 7/31/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import "PayloadItem.h"

@implementation PayloadItem

-(NSString*) description {
    return self.payload.description;
}

@end
