#import "ActivityIndicator.h"
#import <QuartzCore/QuartzCore.h>

@implementation ActivityIndicator

@synthesize spinner = _spinner;

static ActivityIndicator *currentIndicator = nil;

+ (ActivityIndicator *)currentIndicator
{
	if (currentIndicator == nil)
	{
		UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
		
		CGSize size = [[UIScreen mainScreen] bounds].size;
        // NSLog(@"%f,%f",size.height,size.width);
		CGFloat width = size.width;
		CGFloat height = size.height;
		CGRect centeredFrame = CGRectMake(round(keyWindow.bounds.size.width/2 - width/2),
										  round(keyWindow.bounds.size.height/2 - height/2),
										  width,
										  height);
				
		currentIndicator = [[ActivityIndicator alloc] initWithFrame:centeredFrame];
		
		currentIndicator.backgroundColor =  [UIColor clearColor]; //[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
		currentIndicator.opaque = NO;
		currentIndicator.alpha = 0;
		
		currentIndicator.userInteractionEnabled = NO;
	}
	return currentIndicator;
}

- (void) show
{
	[self performSelectorInBackground:@selector(showActivity) withObject:nil];
}
- (void) showActivity
{
    @autoreleasepool
    {
        [self showSpinner];	
        if ([self superview] != [[UIApplication sharedApplication] keyWindow]) 
            [[[UIApplication sharedApplication] keyWindow] addSubview:self];
        
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        
        self.alpha = 1;
        
        [UIView commitAnimations];
    }
}

- (void)showSpinner
{	
	if (self.spinner == nil)
	{
		Box = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 102, 86)];
		Box.center = currentIndicator.center;
		[Box setBackgroundColor:[UIColor blackColor]];
		Box.layer.cornerRadius = 10.0;
		[Box.layer setBorderColor: [[UIColor blackColor] CGColor]];
		[Box.layer setBorderWidth:2.0];
		Box.alpha = 0.3;
		[currentIndicator addSubview:Box];
		
		lblLoading = [[UILabel alloc] init];
		[lblLoading setBackgroundColor:[UIColor clearColor]];
		[lblLoading setTextColor:[UIColor whiteColor]];
		CGPoint point =  currentIndicator.center;
		lblLoading.frame = CGRectMake(point.x - 30, point.y + 20, 102, 20);
		lblLoading.text = @"";
		lblLoading.alpha = 0.8;
		[currentIndicator addSubview:lblLoading];
		
		self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		
		self.spinner.frame = CGRectMake(round(self.bounds.size.width/2 - self.spinner.frame.size.width/2),
								   round(self.bounds.size.height/2 - self.spinner.frame.size.height/2),
								   self.spinner.frame.size.width,
								   self.spinner.frame.size.height);
	}
	
	[self addSubview:self.spinner];
	[self.spinner startAnimating];
}

- (void)hideAfterDelay
{
	[self performSelector:@selector(hide) withObject:nil afterDelay:0.4];
}

- (void)showAfterDelay
{
    [self performSelector:@selector(show) withObject:nil afterDelay:0.2];
}

- (void)hide
{
	
	if([[UIApplication sharedApplication] isIgnoringInteractionEvents])
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(hidden)];
	
	self.alpha = 0;
	
	[UIView commitAnimations];
}

- (void)hidden
{
	if (currentIndicator.alpha > 0)
		return;
	
	[currentIndicator removeFromSuperview];
	currentIndicator = nil;
}


@end
