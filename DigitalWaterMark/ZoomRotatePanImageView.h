//
//  ZoomRotatePanImageView.h
// 
//
//  Created by bennythemink on 20/07/12.
//  Copyright (c) 2012 bennythemink. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZoomAndPanDelegate <NSObject>

-(void)method_pan:(CGRect)rect obj:(UIImageView *)imageView_;
-(void)method_resetFrames:(CGRect)rect obj:(UIImageView *)imageView_;
-(void)method_gestureEnded;

@end

@interface ZoomRotatePanImageView : UIImageView <UIGestureRecognizerDelegate>
{
@protected
    UIPinchGestureRecognizer *_pinchRecogniser;
    UIRotationGestureRecognizer *_rotateRecogniser;
    UIPanGestureRecognizer *_panRecogniser;
    UITapGestureRecognizer *_tapRecogniser;
}

@property (weak) id <ZoomAndPanDelegate> delegate;


- (void) reset;
- (void) resetWithAnimation:(BOOL)animation;

@end
