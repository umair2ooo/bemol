//
//  PayloadToPayoffResolver.h
//  Digimarc Mobile SDK: DMSDemo
//
//  Application level wrapper to manage translating incoming watermark and barcode payload values,
//  to related user visible metadata.  This is done via the DMResolver query services to the 
// 	Digimarc Discover Identity Manager.
//
//  Created by localTstewart on 8/23/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMResolver.h"

@class PayloadToPayoffResolver;

@protocol PayloadToPayoffResolverDelegate <NSObject>
@required
- (void) payloadResolver:(PayloadToPayoffResolver*)p2pr resolvedNewPayoffWithResult:(DMResolveResult*)result;
- (void) payloadResolver:(PayloadToPayoffResolver*)p2pr reportedError:(NSError*)error;
- (void) payloadResolver:(PayloadToPayoffResolver*)p2pr reportedWarningFrom:(NSString*)soure message:(NSString*)msg;
@end


@interface PayloadToPayoffResolver : NSObject

@property (weak) id<PayloadToPayoffResolverDelegate> delegate;
@property (weak) DMResolver* resolver;
@property BOOL resolverIsAvailable;

- (void) updateResolverIsAvailable;
- (void) resolvePayload:(DMPayload*)payload;

@end
