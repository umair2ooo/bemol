//
//  AudioVisualizer.h
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/11/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AudioVisualizer : UIView

@property CGFloat volume;

-(void)setVisualizerDataWithData:(float*)audioData count:(int)count;

@end
