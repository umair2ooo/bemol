#import <UIKit/UIKit.h>

#import "PayoffDatabase.h"
#import "DMResolverSettings.h"
#import "PayoffDoc.h"
#import "DMPayload.h"

@interface ScannedHistoryViewController : UIViewController
{
}
@property (retain, nonatomic) NSMutableArray *payoffsHistory;
@property(nonatomic, strong)PayoffItem *payOff_item;

@end