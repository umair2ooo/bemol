#import <UIKit/UIKit.h>

@interface UIImage (UIImage_DrawText)
{
}

+(UIImage*) drawText:(NSString*) text inImage:(UIImage*)  image atPoint:(CGPoint)   point;

+ (UIImage *)imageWithColor:(UIColor *)color;

@end