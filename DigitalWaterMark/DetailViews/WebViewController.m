/*************************************************************************************************** 
 
 
 The technology detailed in this software is the subject of various pending and issued patents, 
 both internationally and in the United States, including one or more of the following patents: 
 
 5,636,292 C1; 5,710,834; 5,832,119 C1; 6,286,036; 6,311,214; 6,353,672; 6,381,341; 6,400,827; 
 6,516,079; 6,580,808; 6,614,914; 6,647,128; 6,681,029; 6,700,990; 6,704,869; 6,813,366; 
 6,879,701; 6,988,202; 7,003,132; 7,013,021; 7,054,465; 7,068,811; 7,068,812; 7,072,487; 
 7,116,781; 7,158,654; 7,280,672; 7,349,552; 7,369,678; 7,461,136; 7,564,992; 7,567,686; 
 7,590,259; 7,657,057; 7,672,477; 7,720,249; 7,751,588; and EP 1137251 B1; EP 0824821 B1; 
 and JP-3949679, all owned by Digimarc Corporation. 
 
 Use of such technology requires a license from Digimarc Corporation, USA.  Receipt of this software 
 conveys no license under the foregoing patents, nor under any of Digimarc’s other patent, trademark, 
 or copyright rights. 
 
 This software comprises CONFIDENTIAL INFORMATION, including TRADE SECRETS, of Digimarc Corporation, 
 USA, and is protected by a license agreement and/or non-disclosure agreement with Digimarc.  It is 
 important that this software be used, copied and/or disclosed only in accordance with such 
 agreements. 
 
 © Copyright, Digimarc Corporation, USA.  All Rights Reserved. 
 
 ***************************************************************************************************/

#import "WebViewController.h"

@interface WebViewController()

@end

@implementation WebViewController

@synthesize url, webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)dealloc
{
    self.webView.delegate=nil;
    if(m_isLoading)
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *webViewBackground = [[UIView alloc] initWithFrame:self.webView.bounds];
    webViewBackground.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    webViewBackground.backgroundColor = [UIColor lightGrayColor];
    [self.webView insertSubview:webViewBackground belowSubview:self.webView.scrollView];
    
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.webView.delegate = self;
    self.webView.scalesPageToFit = YES;
    self.webView.allowsInlineMediaPlayback = YES;
    m_isLoading = YES;

    [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
}

-(void)viewWillLayoutSubviews {
    CGRect f = self.webView.frame;
    f.size.height = self.view.bounds.size.height;
    self.webView.frame = f;
    self.webView.scrollView.contentInset = self.webView.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(self.topLayoutGuide.length,0,0,0);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return NO;
}

//Old pre-iOS 6 callback
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    BOOL value = ([self supportedInterfaceOrientations] & (1 << interfaceOrientation)) != 0;
//    return value;
//}

//new callback
- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    else
    {
        return UIInterfaceOrientationMaskAll;
    }
}

-(void)removeFromScreen {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webViewThatFinished
{
    m_isLoading =  NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    NSString *title = [(UIWebView *)self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.navigationItem.title = title;
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *lowercaseScheme = [[[request URL] scheme] lowercaseString];
    
    //if it's not http or https, pass it along to safari
    if(![lowercaseScheme isEqualToString:@"http"]&&![lowercaseScheme isEqualToString:@"https"]&&![lowercaseScheme isEqualToString:@"file"]&&![lowercaseScheme isEqualToString:@"about"])
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //we don't want to error if it's just a runtime error, and not a connection error
    //connection error is 1009
    //Background: some payoffs have runtime errors, like cannot load plugin errors
    //even though they actually work. We don't want to break those.
    if([error code]!=-1009 && [error code]!=-1003)
        return;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;    
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Network Error"
                                                    message:error.description
                                                    delegate:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil];
    [alert show];
    m_isLoading =  NO;
}

@end
