/***************************************************************************************************
 
 The technology detailed in this software is the subject of various pending and issued patents,
 both internationally and in the United States, including one or more of the following patents:
 
 5,636,292 C1; 5,710,834; 5,832,119 C1; 6,286,036; 6,311,214; 6,353,672; 6,381,341; 6,400,827;
 6,516,079; 6,580,808; 6,614,914; 6,647,128; 6,681,029; 6,700,990; 6,704,869; 6,813,366;
 6,879,701; 6,988,202; 7,003,132; 7,013,021; 7,054,465; 7,068,811; 7,068,812; 7,072,487;
 7,116,781; 7,158,654; 7,280,672; 7,349,552; 7,369,678; 7,461,136; 7,564,992; 7,567,686;
 7,590,259; 7,657,057; 7,672,477; 7,720,249; 7,751,588; and EP 1137251 B1; EP 0824821 B1;
 and JP-3949679, all owned by Digimarc Corporation.
 
 Use of such technology requires a license from Digimarc Corporation, USA.  Receipt of this software
 conveys no license under the foregoing patents, nor under any of Digimarc’s other patent, trademark,
 or copyright rights.
 
 This software comprises CONFIDENTIAL INFORMATION, including TRADE SECRETS, of Digimarc Corporation,
 USA, and is protected by a license agreement and/or non-disclosure agreement with Digimarc.  It is
 important that this software be used, copied and/or disclosed only in accordance with such
 agreements.
 
 © Copyright, Digimarc Corporation, USA.  All Rights Reserved.
 
 ***************************************************************************************************/
/// @file DMPayload+Internals.h

#import <Foundation/Foundation.h>
#import "DMPayload.h"

/** @defgroup payloadInternals DMPayload Internals
* Implementation details for the semi-opaque DMPayload class.
*
* In most cases applications may ignore this level of detail, and simply store DMPayload
* notifications, and at some point route them through the DMResolver resolve query API.
*
* However test applications, or cases where special applications will require ability to
* see and or parse the payload CPM content may be facilitated using these methods.
*
* CPM refers to the Digimarc Common Payload Model, defined elsewhere with its own toolkit
* and documentation package.   This document's scope is limited to the use of CPM in 
* identifying watermark payload values read from image or audio source media, and reported
* via the DMSDK.
*
* @ingroup DMPayloadGroup
*/

@interface DMPayload()

/** Temporary storage for parsed payload type identifier.
*
* Actual type information is contained within the cpmPath string.
*
* DEPRECTED -- replaced with payloadCategory, category attribute query methods, and other CPM attribute queries defined in this interface.
*
* @ingroup payloadInternals
*/
@property (readonly) int payloadType;

/** Temporary storage for parsed payload category identifier.
*
* Actual type information is contained within the cpmPath string.
*
* @ingroup payloadInternals
*/
@property (readonly) int payloadCategory;

/** The fully qualified CPM payload path.
*
* Only the cpmPath is actually reported by DMSDK readers.  This single string contains a full identification
* of the payload type, security key context, and value.
*
* The cpmPath will be automatically generated when reading in legacy payloadType and payloadData
* from earlier versions of the DMPayload object.  This allows users to seamlessly upgrade Digimarc Discover
* detection history payload database information into the current DMSDK format.
*
* @ingroup payloadInternals
*/
@property (readonly, retain) NSString* cpmPath;

/** The effective number of data bits contained in this payload.
*
* This information is parsed from the fully qualified protocol and version type information in cpmPath.
* @ingroup payloadInternals
*/
@property (readonly) int numberValueBits;

/** The grid portion of the fully qualified cpmPath
* 
* Grids identify proprietary media processing algorithms used within the Digimarc image and audio readers.
* In some cases the grid used to detect a watermark informs the process of identifying associated payoff materials.
*
* The most common grids are "GC41" for image watermarks, "Barcode" for 1D barcodes and QRcodes, and "Audio" for audio watermarks.
* @ingroup payloadInternals
*/
@property (readonly) NSString* grid;

/** The protocol portion of the fully qualified cpmPath.
*
* Protocol + Version information identifies a specific data encoding scheme, including the number of effective
* data bits in the final payload value.
* The most common protocols are "KE" for image watermarks, "AFRE" or "TDS4" for audio watermarks.
* @ingroup payloadInternals
*/
@property (readonly) NSString* protocol;

/** The protocol version portion of the fully qualified cpmPath.
*
* Protocol + Version information identifies a specific data encoding scheme, including the number of effective
* data bits in the final payload value.
*
* The protocol version is typically expressed as "v#" string where # is the version number.   Common image watermark
* versions can be seen in the names of the various DMPayloadType enum values.  Barcode and audio watermark versions are all
* currently shown as "0" or "v0".
*
* @ingroup payloadInternals
*/
@property (readonly) NSString* protocolVersion;

/** The key string identifies the catalog name of an encryption key registered within the CPM library.
*
* In some cases, a customized watermark encryption key will inform payload to payoff resolving to use an alternate,
* OEM-specific lookup method.   Watermark encryption keys, embedding with keys, and reading with customized keys
* require additional Digimarc technical customer support, and is available only on a limited basis to meet specific
* customer requirements.
*
* @ingroup payloadInternals
*/
@property (readonly) NSString* key;

/** The payload value portion of the fully qualified cpmPath
*
* The payload value string format varies by the payload type, grid, protocol, and version.
*	- ASCII hex string for image and audio watermarks
*	- ASCII decimal string for 1D barcodes
*	- UTF-8 or UTF-16 value string, often a URL, for QR codes
*
* @ingroup payloadInternals
*/
@property (readonly) NSString* valueString;

/** The Digimarc Resolver API v2 query string to use for this payload within DMResolver
*
* DMPayload understands and works with the Digimarc Resolver API v2 syntax to identify the payload to be resolved.
* DMResolver wraps this fragment with network connection, query packet construction & authentication, async I/O, 
* and handling of the resolve result.   DMResolver does not otherwise contain deep knowledge of cpmPaths or 
* payload internals.
*
* @ingroup payloadInternals
*/
@property (readonly) NSString* resolverResourceId;

/** Query logical bits based on CPM protocol and version strings
*
* Note this is a generic utility function, and is not tied to the specific cpmPath of this payload object.
*
* @ingroup payloadInternals
*/
- (int) numberValueBitsForProtocol:(NSString*)protocol version:(NSString*)version;


@end
