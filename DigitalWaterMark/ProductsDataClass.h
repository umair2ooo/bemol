#import <Foundation/Foundation.h>

@interface ProductsDataClass : NSObject

@property (nonatomic, strong)NSString *p_id;
@property (nonatomic, strong)NSString *p_name;
@property (nonatomic) int p_rating;
@property (nonatomic, strong)NSString *p_price;
@property (nonatomic, strong)NSString *p_sale;
@property (nonatomic, strong)NSString *p_inStock;
@property (nonatomic, strong)NSString *p_model;
@property (nonatomic, strong)NSString *p_skuNumber;
@property (nonatomic, strong)NSString *p_description;
@property (nonatomic, strong)NSURL *p_imageURL;
@property (nonatomic, strong)NSURL *p_imageURL_AR;

//color, size, totalQuantity, reviews
@property (nonatomic, strong)NSArray *p_colorURL_array;
@property (nonatomic, strong)NSArray *p_size_array;
@property (nonatomic)int p_totalQuantity;
@property (nonatomic)int p_quantityRequired;
@property (nonatomic, strong)NSString *p_quantity;
@property (nonatomic, strong)NSString *p_reviews;
@property (nonatomic, strong)NSString *p_colorName;
@property (nonatomic, strong)NSString *p_subTotal;
@property (nonatomic, strong)NSString *p_subTotalCurrency;
@property (nonatomic, strong)NSString *p_orderItemId;

@property(nonatomic, strong)NSMutableArray *array_location;
@property(nonatomic, strong)NSMutableArray *p_array_colors;
@property(nonatomic, strong)NSMutableArray *p_array_stores;
@end