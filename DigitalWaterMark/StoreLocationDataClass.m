//
//  StoreLocationDataClass.m
//  DigitalWaterMark
//
//  Created by Fahim Bilwani on 3/5/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "StoreLocationDataClass.h"

@implementation StoreLocationDataClass

@synthesize store_name;
@synthesize store_address;
@synthesize store_distance;
@synthesize store_latitude;
@synthesize store_longitude;
@synthesize store_array_Details;

-(void)setStore_name:(NSString *)store_namex
{
    if (store_namex)
    {
        store_name = store_namex;
    }
    else
    {
        store_name = nil;
    }
}


-(void)setStore_address:(NSString *)store_addressx
{
    if (store_addressx)
    {
        store_address = store_addressx;
    }
    else
    {
        store_address = nil;
    }
}


-(void)setStore_distance:(NSString *)store_distancex
{
    if (store_distancex)
    {
        store_distance = store_distancex;
    }
    else
    {
        store_distance = nil;
    }
}


-(void)setStore_latitude:(NSString *)store_latitudex
{
    if (store_latitudex)
    {
        store_latitude = store_latitudex;
    }
    else
    {
        store_latitude = nil;
    }
}
-(void)setStore_longitude:(NSString *)store_longitudex
{
    if (store_longitudex)
    {
        store_longitude = store_longitudex;
    }
    else
    {
        store_longitude = nil;
    }
}

-(void)setArray_Storedetails:(NSMutableArray *)array_storedetailsx
{
    if (array_storedetailsx)
    {
        store_array_Details = [[NSMutableArray alloc] initWithArray:array_storedetailsx];
        
    }
}
@end
