//
//  PayoffDatabase.m
//  DMSDemo
//
//  Created by Sinclair, Eoin on 3/1/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import "PayoffDatabase.h"
#import "PayoffDoc.h"

@implementation PayoffDatabase

+ (NSString *)getPrivateDocsDir {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Private Documents"];
    
    NSError *error;
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:&error];
    
    return documentsDirectory;
}

+ (NSMutableArray *)loadPayoffDocs {
    NSString *documentsDirectory = [PayoffDatabase getPrivateDocsDir];
    
    NSError *error;
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
    if (!files) {
        NSLog(@"Error reading contents of documents directory: %@", [error localizedDescription]);
        return nil;
    }
    
    NSMutableArray *retval = [NSMutableArray arrayWithCapacity:[files count]];
    for (NSString *f in files) {
        if ([[f pathExtension] compare:@"payoffitem" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:f];
            PayoffDoc *doc = [[PayoffDoc alloc] initWithDocPath:fullPath];
            [retval addObject:doc];
        }
    }
    
    return retval;
}

+ (NSString *)nextPayoffDocPath {
    NSString *documentsDirectory = [PayoffDatabase getPrivateDocsDir];
    
    NSError *error;
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
    if (!files) {
        NSLog(@"Error reading contents of documents directory: %@", [error localizedDescription]);
        return nil;
    }
    
    int maxNumber = 0;
    for (NSString *f in files) {
        if ([f.pathExtension compare:@"payoffitem" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            NSString *fileName = [f stringByDeletingPathExtension];
            maxNumber = MAX(maxNumber, fileName.intValue);
        }
    }
    
    NSString *availableName = [NSString stringWithFormat:@"%d.payoffitem", maxNumber+1];
    return [documentsDirectory stringByAppendingPathComponent:availableName];
    
}

@end
