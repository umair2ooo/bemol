/*************************************************************
 
 The technology detailed in this software is the subject of various pending and issued patents,
 both internationally and in the United States, including one or more of the following patents:
 
 5,636,292 C1; 5,710,834; 5,832,119 C1; 6,286,036; 6,311,214; 6,353,672; 6,381,341; 6,400,827;
 6,516,079; 6,580,808; 6,614,914; 6,647,128; 6,681,029; 6,700,990; 6,704,869; 6,813,366;
 6,879,701; 6,988,202; 7,003,132; 7,013,021; 7,054,465; 7,068,811; 7,068,812; 7,072,487;
 7,116,781; 7,158,654; 7,280,672; 7,349,552; 7,369,678; 7,461,136; 7,564,992; 7,567,686;
 7,590,259; 7,657,057; 7,672,477; 7,720,249; 7,751,588; and EP 1137251 B1; EP 0824821 B1;
 and JP-3949679, all owned by Digimarc Corporation.
 
 Use of such technology requires a license from Digimarc Corporation, USA.  Receipt of this software
 conveys no license under the foregoing patents, nor under any of Digimarc’s other patent, trademark,
 or copyright rights.
 
 This software comprises CONFIDENTIAL INFORMATION, including TRADE SECRETS, of Digimarc Corporation,
 USA, and is protected by a license agreement and/or non-disclosure agreement with Digimarc.  It is
 important that this software be used, copied and/or disclosed only in accordance with such
 agreements.
 
 © Copyright, Digimarc Corporation, USA.  All Rights Reserved.
 
*************************************************************/
/// @file DMPayload.h

/** @class DMPayload
* See the @ref DMPayloadGroup "DMPayload API" for complete documentation of this class.
* 
*/
/** @defgroup DMPayloadGroup DMPayload API
*
* DMPayload class encapsulates the payloads detected from DMSDK scanning of image or audio media.
* 
* DMPayload is designed so that an application can manage it easily and provide it directly to the DMResolver component for resolution. 
* In general, applications have no need to inspect a DMPayload object beyond possibly comparing it for equality to another DMPayload object.
* A payload equality comparitor, and simple payload category attribute identifying which general type of payload this item represents
* are provided by this interface.  Additional lower level payload content details are available in the DMPayload+Internals.h category.
*
* DMPayload also implements the NSCoding protocol, so that DMPayload objects may be easily archived and unarchived into
* core data dbs or other storage containers.  This is often used by applications to build audit trails
* or "recently visited" history lists.
*/

/**
* The types of payloads
* 
* @deprecated (DMSDK 1.2.0) DMPayloadType codes are being phased out to be replaced with DMPayloadCategory groups,
* along with detailed payload inspection methods available within DMPayload+Internals.h.  DMPayloadType will remain
* available for now, but will be removed in a future DMSDK release.
*
* The specific type numbers shown here are assigned by DDIM resolver v2 API (for types < 1000).
*
* @ingroup DMPayloadGroup
*/
typedef enum DMPayloadType
{
    DMPayloadTypeMobileImage = 5,		///< Digimarc 32-bit Classic or Chroma watermarks
    DMPayloadTypeMobileImageV6 = 6,		///< Digimarc 64-bit Classic or Chroma watermarks
    DMPayloadTypeMobileImageV7 = 7,		///< Digimarc 47-bit Packaging watermarks, for GTIN-12, GTIN-14
    DMPayloadTypeMobileImageV8 = 8,		///< Digimarc 64-bit Packaging watermarks, for GTIN-12, GTIN-14 + extra metadata
    DMPayloadTypeMobileImageV9 = 9,		///< Digimarc 98-bit Packaging watermarks, for SGTIN
    DMPayloadTypeBarcode = 253,			///< Standard 1D barcode types
    DMPayloadTypeAudioAFRE = 254,		///< Digimarc audio watermarks, as used in DDIM online audio embedding
    DMPayloadTypeAudioTDS4 = 255,		///< Digimarc audio watermarks, as used in some specialized applications
    DMPayloadTypeUnknown = 1000,		///< Unknown payload format, not resolvable
    DMPayloadTypeQRcode = 1001			///< Standard QR code barcodes.  Payload value may contain URL.  Not resolvable with DDIM.
} DMPayloadType;

/**
 * Payload Categories
 *
 * These categories provide simplified organization of all payload types from a common source.   Typical usage would be to 
 * tie payloads detected to input source, sensor, or UI activity that generated them.  For example, in Discover the
 * payload category defines which icon to display in payoff history lists, when the payoff itself does not provide a thumbnail image.
 *
 * Payload Categories, combined with the detailed payload inspection methods in DMPayload+Internals.h, replace the deprecated DMPayloadType codes.
 *
 * @ingroup DMPayloadGroup
 */
typedef enum DMPayloadCategory
{
    DMPayloadCategoryImage = 1,         ///< All Digimarc Image Watermark types
    DMPayloadCategoryAudio = 2,         ///< All Digimarc Audio Watermark types
    DMPayloadCategoryBarcode = 3,       ///< All Barcode types, including 1D barcodes and QRcodes.  See also isQRCode attribute.
    DMPayloadCategoryUnknown = 1000
} DMPayloadCategory;

@interface DMPayload : NSObject <NSCoding>

/** Returns the type of payload
*
* @deprecated (DMSDK 1.2.0) See comments in typedef for DMPayloadType.
*
* @ingroup DMPayloadGroup
*/
- (DMPayloadType) getPayloadType;

/** Returns the group or category of reader that detected this payload.
*
* See comments in typedef for DMPayloadCategory.  Also see related specific category query methods isImage, isAudio, isBarcode, isQRCode.
*
* @ingroup DMPayloadGroup
*/
- (DMPayloadCategory) getPayloadCategory;

/** Is this payload a member of the DMPayloadCategoryImage group ?
*
* @returns YES for all variations of Digimarc Image Watermarks.  Else NO.
* @ingroup DMPayloadGroup
*/
- (BOOL) isImage;

/** Is this payload a member of the DMPayloadCategoryAudio group ?
 *
 * @returns YES for all variations of Digimarc Audio Watermarks.  Else NO.
 * @ingroup DMPayloadGroup
 */
- (BOOL) isAudio;

/** Is this payload a member of the DMPayloadCategoryBarcode group ?
 *
 * @returns YES for all variations of Barcodes, including 1D codes, and QRcodes.  Else NO.
 * @ingroup DMPayloadGroup
 */
- (BOOL) isBarcode;

/** Is this payload a member of the DMPayloadCategoryBarcode group, and is specifically a QRCode ?
 *
 * @returns YES for QRCodes only.  This is often needed to detect and route QRCode detections through a different payload to payoff resolving method than other payload types.
 * @ingroup DMPayloadGroup
 */
- (BOOL) isQRCode;

/** Create an instance of DMPayload with the correct type.
* @ingroup DMPayloadGroup
*/
- (id) initWithCpmPath:(NSString*)cpmPath;

/** Convenience method exposed for apple native barcode reader in app-defined image source.
* @ingroup DMPayloadGroup
*/
- (id) initWithBarcode:(NSString*)barcodeContent;

/** Convenience method exposed for apple native barcode reader in app-defined image source.
* @ingroup DMPayloadGroup
*/
- (id) initWithQRcode:(NSString*)qrcodeContent;

/** Compare incoming payload with others to test for duplicates.
* 
* This method is useful when comparing an incoming payload to a cache of one or more previously reported payloads.  
* The compare operand accepts any NSObject* or nil, so that objects of mixed types may be stored in the same cache container.
*
* For example metadata dictionaries returned from the Apple barcode reader may be stored in the same container as 
* DMPayload objects.
*
* @param objectToCompare Any NSObject or nil.
* @result YES if objectToCompare is also a DMPayload object, with logically equal payload value, NO otherwise. 
*
* @ingroup DMPayloadGroup
*/
- (BOOL) isEqual:(id)objectToCompare;

/** NSCoding support -- archiving
* DMPayload always stores its current internal format, consisting of a compound, fully qualified cpmPath string.
*
* The cpmPath string contains precise payload type, protocol, version, and value.
* @ingroup DMPayloadGroup
*/
- (void)encodeWithCoder:(NSCoder *)encoder;

/** NSCoding support -- unarchiving
* DMPayload looks for its preferred cpmPath string format in the incoming archive.  If not available, it will go
* on to look for legacy forms of DMPayload type and data array objects.   The resulting DMPayload object
* in either case is converted to current version DMPayload cpmPath string.
*
* This allows user applications built with previous generation DM Mobile Image SDKs, including Digimarc Discover
* and white label variations, to be able to upgrade in place, migrating any archived user payload history to
* current DMPayload format without having to provide a data converter.
* @ingroup DMPayloadGroup
*/
- (id)initWithCoder:(NSCoder *)aDecoder;

@end
