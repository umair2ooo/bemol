#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ActivityIndicator : UIView
{

	UIActivityIndicatorView *spinner;
	
	UILabel *lblLoading;
	UIView *Box;
}


@property (nonatomic, retain) UIActivityIndicatorView *spinner;

+ (ActivityIndicator *)currentIndicator;

- (void) showActivity;
- (void) show;
- (void) showSpinner;
- (void) hideAfterDelay;
- (void)showAfterDelay;
- (void) hide;
- (void) hidden;

@end