#import "Cell_shoppingCart.h"

@implementation Cell_shoppingCart

- (void)awakeFromNib
{
    // Initialization code
    DLog(@"Initialization code");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - action_remove
- (IBAction)action_remove:(id)sender
{
    [self.delegate method_deleteObject:self.tag];
}



#pragma mark - action_edit
- (IBAction)action_edit:(id)sender
{
    [self.delegate method_editObject:self.tag];
}



@end