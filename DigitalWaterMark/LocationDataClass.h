#import <Foundation/Foundation.h>

@interface LocationDataClass : NSObject


@property (nonatomic, strong)NSString *location_storeName;
@property (nonatomic, strong)NSURL *location_storeImageURL;
@property (nonatomic)float location_lat;
@property (nonatomic)float location_long;
@property (nonatomic, assign) BOOL location_nearestStore;
@property (nonatomic, strong)NSString *location_Asle;
@property (nonatomic, strong)NSString *location_Bay;

@end