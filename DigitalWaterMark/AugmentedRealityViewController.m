#import "AugmentedRealityViewController.h"

#import "ZoomRotatePanImageView.h"
#import "Singleton.h"
#import "RXCustomTabBar.h"

#define optionViewWidth 150.0
#define optionViewHeight 50.0
#define K_PAUSE_INTERVAL 2.0

@interface AugmentedRealityViewController ()<ZoomAndPanDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    BOOL bool_statusBar;
}
@property(nonatomic, strong)NSTimer *myTimer;
@property(nonatomic, strong)UIImage *image_flipped;
@property(nonatomic, strong)UIImageView *imageView_currentImage;
@property (nonatomic) UIImagePickerController *imagePickerController;
@property (nonatomic) NSMutableArray *array_capturedImages;

@property (weak, nonatomic) IBOutlet UIImageView *imageView_backImage;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBaar_;

@property (strong, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet ZoomRotatePanImageView *viewSimple_;
@property (weak, nonatomic) IBOutlet UIView *view_options;



- (IBAction)action_photoLibrary:(id)sender;
- (IBAction)action_camera:(id)sender;
- (IBAction)action_front_rear_camera:(id)sender;

- (IBAction)action_cancel:(id)sender;
- (IBAction)action_capture:(id)sender;
- (IBAction)action_addNewImage:(id)sender;

- (IBAction)action_flip:(id)sender;
- (IBAction)action_flipFromCamera:(id)sender;


@end




@implementation AugmentedRealityViewController



-(void)method_addLayerToObject:(CALayer *)layer_
{
    [layer_ setBorderColor: [[UIColor whiteColor] CGColor]];
    [layer_ setBorderWidth: 0.5];
    [layer_ setShadowOffset:CGSizeMake(-3.0, 3.0)];
    [layer_ setShadowRadius:3.0];
    [layer_ setShadowOpacity:1.0];
    [layer_ setShadowColor:[[UIColor whiteColor] CGColor]];
}

#pragma mark - method_makeRotatingImage
-(void)method_makeRotatingImage
{
    Singleton *single = [Singleton retriveSingleton];
    
    
    for (id x in self.view.subviews)
    {
        if ([x isKindOfClass:[ZoomRotatePanImageView class]])
        {
            [x removeFromSuperview];
        }
    }
    
    
    ZoomRotatePanImageView *imageView = [[ZoomRotatePanImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    imageView.delegate = self;
    [imageView setImage:single.image_ARImage];
    [imageView setBackgroundColor:[UIColor clearColor]];
    imageView.center = self.view.center;
    
    
    
    [self.view addSubview:imageView];

    if ([self.array_capturedImages count])
    {
        [self.array_capturedImages removeAllObjects];
    }
    
    [self.array_capturedImages insertObject:imageView atIndex:0];
    
    imageView = nil;
    
}

#pragma mark - load image from URL
-(void)method_loadImageFromURL
{
    Singleton *single = [Singleton retriveSingleton];
    
    
    if (!single.image_ARImage)
    {
        if (single.string_imageURL)
        {
            // download the image asynchronously
            [self downloadImageWithURL:single.string_imageURL
                       completionBlock:^(BOOL succeeded, UIImage *image) {
                           
                           
                           single.image_ARImage = image;
                           
                           
                           [self method_makeRotatingImage];
                       }];
        }
    }
    else
    {
        [self method_makeRotatingImage];
    }
}


#pragma mark - method_updateFrame
-(void)method_updateFrame
{
    [self.toolBaar_ setFrame:CGRectMake(0,
                                        self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-44,
                                        self.view.frame.size.width,
                                        self.toolBaar_.frame.size.height)];
}


#pragma mark - cycle
-(void)viewWillAppear:(BOOL)animated
{
    [self method_statusBarHideAndShow];
    
    if (self.imagePickerController)
    {
        [self.imagePickerController.view removeFromSuperview];
        self.imagePickerController = nil;
    }



//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(receiveTestNotification:)
//                                                 name:@k_notification
//                                               object:nil];


    for (id x in self.view.subviews)
    {
        if ([x isKindOfClass:[ZoomRotatePanImageView class]])
        {
            [x removeFromSuperview];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self method_loadImageFromURL];
}



//-(void)viewWillLayoutSubviews
//{
//    [self.toolBaar_ setFrame:CGRectMake(0, 50, 320, 35)];
//}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageView_backImage.image = nil;
    bool_statusBar = false;
    
//    [self method_updateFrame];

    
    self.title = @"Augmented Reality Demo";
    
    [self method_addLayerToObject:self.view_options.layer];
    
        self.imageView_backImage.userInteractionEnabled = YES;
    
//    To hide and show Navigation Bar and tabBar
    
    /*
     //        UITapGestureRecognizer *tapRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(method_handleTap:)];
     //        [self.imageView_backImage addGestureRecognizer:tapRecogniser];
     */
    
    
    
    self.array_capturedImages = [[NSMutableArray alloc] init];
    [self.array_capturedImages addObject:self.view_options];
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        // There is not a camera on this device, so don't show the camera button.
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"There is no camera in this device"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }

    self.viewSimple_.delegate = self;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - download image from URL
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}



#pragma mark - add new image
- (IBAction)action_addNewImage:(id)sender
{
    ZoomRotatePanImageView *imageView = [[ZoomRotatePanImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    imageView.delegate = self;
    [imageView setBackgroundColor:[UIColor lightGrayColor]];
    imageView.center = self.view.center;
    
    [self.view addSubview:imageView];
    
    [self.array_capturedImages insertObject:imageView atIndex:0];
    //    [self.array_capturedImages addObject:imageView];
    
    imageView = nil;
}


#pragma mark - action_flip
- (IBAction)action_flip:(id)sender
{
    self.imageView_currentImage.image = [UIImage imageWithCGImage:self.imageView_currentImage.image.CGImage
                                                            scale:self.imageView_currentImage.image.scale
                                                      orientation:(self.imageView_currentImage.image.imageOrientation == 0)?4:0];
    
    //      self.imageView_currentImage.transform = CGAffineTransformScale(self.imageView_currentImage.transform, -1.0, 1.0);
}

- (IBAction)action_flipFromCamera:(id)sender
{
    self.imageView_currentImage.image = [UIImage imageWithCGImage:self.imageView_currentImage.image.CGImage
                                                            scale:self.imageView_currentImage.image.scale
                                                      orientation:(self.imageView_currentImage.image.imageOrientation == 0)?4:0];
}


#pragma mark - action_photoLibrary
- (IBAction)action_photoLibrary:(id)sender
{
    bool_statusBar = true;
    
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

#pragma mark - action_camera
- (IBAction)action_camera:(id)sender
{
    bool_statusBar = YES;
    
    
    [self method_statusBarHideAndShow];
    
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
}


#pragma mark - method_statusBarHideAndShow
-(void)method_statusBarHideAndShow
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        // iOS 7
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

#pragma mark - prefersStatusBarHidden
- (BOOL)prefersStatusBarHidden
{
    return bool_statusBar;
}


#pragma mark - action_front_rear_camera
- (IBAction)action_front_rear_camera:(id)sender
{
    if (self.imagePickerController.cameraDevice==UIImagePickerControllerCameraDeviceFront)
    {
        self.imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
//        self.imagePickerController.cameraViewTransform = CGAffineTransformIdentity;
    }
    else
    {
        self.imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        
//        self.imagePickerController.cameraViewTransform = CGAffineTransformIdentity;
//        self.imagePickerController.cameraViewTransform = CGAffineTransformScale(self.imagePickerController.cameraViewTransform, -1,     1);
    }
}

//http://stackoverflow.com/questions/18583461/easier-way-to-subview-a-uiimagepickercontroller-mines-not-working
//http://stackoverflow.com/questions/20590346/using-cameraoverlayview-with-uiimagepickercontroller
//https://developer.apple.com/library/ios/samplecode/PhotoPicker/Listings/PhotoPicker_APLViewController_m.html


#pragma mark - showImagePickerForSourceType
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    if (self.imagePickerController)
    {
        self.imagePickerController = nil;
    }
    
    self.imagePickerController = [[UIImagePickerController alloc] init];
    self.imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.imagePickerController.sourceType = sourceType;
    self.imagePickerController.delegate = self;
    
    [self.imagePickerController setAllowsEditing:NO];
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        /*
         The user wants to use the camera interface. Set up our custom overlay view for the camera.
         */
        self.imagePickerController.showsCameraControls = NO;
        
        /*
         Load the overlay view from the OverlayView nib file. Self is the File's Owner for the nib file, so the overlayView outlet is set to the main view in the nib. Pass that view to the image picker controller to use as its overlay view, and set self's reference to the view to nil.
         */
        [[NSBundle mainBundle] loadNibNamed:@"OverlayView" owner:self options:nil];
        self.overlayView.frame = self.imagePickerController.cameraOverlayView.frame;
        
        
        
        [self.array_capturedImages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             [self.overlayView addSubview:obj];
         }];
        
        
        
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        // set the aspect ratio of the camera
        float heightRatio = 4.0f / 3.0f;
        // calculate the height of the camera based on the screen width
        float cameraHeight = screenSize.width * heightRatio;
        // calculate the ratio that the camera height needs to be scaled by
        float scale = screenSize.height / cameraHeight;
        // move the controller to the center of the screen
        self.imagePickerController.cameraViewTransform = CGAffineTransformMakeTranslation(0, (screenSize.height - cameraHeight) / 2.0);
        // concatenate the scale transform
        self.imagePickerController.cameraViewTransform = CGAffineTransformScale(self.imagePickerController.cameraViewTransform, scale, scale);
        
        
        
        
//        self.imagePickerController.cameraViewTransform = CGAffineTransformMakeScale(1.0, 1.03);
        
        
        [self.view_options setHidden:YES];
        
        
        //        [self.overlayView addSubview:self.view_options];
        
        
        
        /*                  to stop zooming                 */
        
        //        self.imagePickerController.cameraOverlayView = self.overlayView;
        [self.imagePickerController.view addSubview:self.overlayView];
        
        
        /*                  to stop zooming                 */
        
//        NSArray *array_temp = [NSArray arrayWithArray:self.overlayView.subviews];
//        
//        for (id x in array_temp)
//        {
//            if ([x isKindOfClass:[UIImageView class]])
//            {
//                [self.overlayView bringSubviewToFront:x];
//            }
//            else if([x isKindOfClass:[ZoomRotatePanImageView class]])
//            {
//                [self.overlayView sendSubviewToBack:x];
//            }
//        }

        
        UIImageView *imageView_grid = [[UIImageView alloc] initWithFrame:self.overlayView.frame];
        [imageView_grid setBackgroundColor:[UIColor clearColor]];
        [imageView_grid setImage:[UIImage imageNamed:@"Grid_"]];
        [self.overlayView addSubview:imageView_grid];
        
        imageView_grid = nil;
        
        self.overlayView = nil;
    }
    
    self.imagePickerController.extendedLayoutIncludesOpaqueBars = YES;
    
    [[self method_getTabBar] presentViewController:self.imagePickerController animated:YES completion:nil];
}


#pragma mark - method_getTabBar
-(id)method_getTabBar
{
    id  parentController = self.parentViewController;
    
//    DLog(@"parentController: %@",[parentController class]);
//    DLog(@"parentController of parentController: %@",[parentController parentViewController]);
    
    if ([parentController isKindOfClass:[RXCustomTabBar class]])
    {
        return parentController;
    }
    else
    {
        return [parentController parentViewController];
    }
}

#pragma mark - action_cancel
- (IBAction)action_cancel:(id)sender
{
    bool_statusBar = false;
    
    [self finishAndUpdate];
}

#pragma mark - action_capture
- (IBAction)action_capture:(id)sender
{
    [self.imagePickerController takePicture];
}


#pragma mark - finishAndUpdate
- (void)finishAndUpdate
{
    bool_statusBar = false;
    
    [[self method_getTabBar] dismissViewControllerAnimated:YES completion:nil];
    
    if ([self.array_capturedImages count] > 0)
    {
        [self.array_capturedImages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             [self.view addSubview:obj];
         }];
    }
    
    [self.view bringSubviewToFront:self.toolBaar_];
    self.imagePickerController = nil;
}



#pragma mark - UIImagePickerControllerDelegate
// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    bool_statusBar = false;
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];

    [self.imageView_backImage setImage:image];
    
    if (self.imagePickerController.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        if (self.imagePickerController.cameraDevice == UIImagePickerControllerCameraDeviceFront)
        {
            self.imageView_backImage.image = [UIImage imageWithCGImage:image.CGImage
                                                                 scale:image.scale
                                                           orientation:UIImageOrientationLeftMirrored];
        }
    }
    
    
    
    
    
    [self finishAndUpdate];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    bool_statusBar = false;

    [[self method_getTabBar] dismissViewControllerAnimated:YES completion:nil];
    self.imagePickerController = nil;
    
//    [[self method_getTabBar] dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - method_hideViews
-(void)method_hideViews:(id)timer
{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^
     {
         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];

         if (self.view_options.alpha == 0)
             self.view_options.alpha=1;
         else
         {
             self.view_options.alpha=0;
         }
     }
                     completion:^(BOOL b)
     {
     }
     ];
}


#pragma mark - ZoomAndPanDelegates
-(void)method_pan:(CGRect)rect obj:(UIImageView *)imageView_
{
    if (self.view_options.alpha == 0)
        self.view_options.alpha = 1;
    
    
    
    if (self.imageView_currentImage != imageView_)
    {
        self.imageView_currentImage = imageView_;
    }
    
    
    if (self.view_options.hidden)
        self.view_options.hidden = NO;
    
    [self.view bringSubviewToFront:self.view_options];
    
    
    
    if (rect.origin.x <= 0 && rect.origin.y <=  self.view_options.frame.size.height)
    {
        self.view_options.frame = CGRectMake(0,
                                             self.view.frame.size.height-self.view_options.frame.size.height,
                                             optionViewWidth,
                                             optionViewHeight);
        return;
    }
    
    
    if (rect.origin.x <= 0)                                             //for x-axis
    {
        self.view_options.frame = CGRectMake(0,
                                             rect.origin.y-self.view_options.frame.size.height,
                                             optionViewWidth,
                                             optionViewHeight);
        return;
    }
    
    
    if (rect.origin.y <=  self.view_options.frame.size.height)          //for y-axis
    {
        self.view_options.frame = CGRectMake(rect.origin.x+(rect.size.width-self.view_options.frame.size.width) / 2,
                                             0,
                                             optionViewWidth,
                                             optionViewHeight);
        return;
    }
    
    
    self.view_options.frame = CGRectMake(rect.origin.x+(rect.size.width-self.view_options.frame.size.width) / 2,
                                         rect.origin.y-self.view_options.frame.size.height,
                                         optionViewWidth,
                                         optionViewHeight);
}


-(void)method_resetFrames:(CGRect)rect obj:(UIImageView *)imageView_
{
    if (self.view_options.alpha == 0)
        self.view_options.alpha = 1;

    
    
    if (self.imageView_currentImage != imageView_)
    {
        self.imageView_currentImage = imageView_;
    }
    
    
    [self.view bringSubviewToFront:self.view_options];
    
    [UIView animateWithDuration:.25 animations:^{
        self.view_options.transform = CGAffineTransformIdentity;
        self.view_options.frame = CGRectMake(rect.origin.x+(rect.size.width-self.view_options.frame.size.width) / 2,
                                             rect.origin.y-self.view_options.frame.size.height,
                                             self.view_options.frame.size.width,
                                             self.view_options.frame.size.height);
    }];
}

-(void)method_gestureEnded
{
    if(self.myTimer)
        [self.myTimer invalidate];
    
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:K_PAUSE_INTERVAL
                                                    target:self
                                                  selector:@selector(method_hideViews:)
                                                  userInfo:nil
                                                   repeats:NO];
}




#pragma tap gesture
- (void)method_handleTap:(UITapGestureRecognizer*)recogniser
{
    NSLog(@"tag = %ld", (long)[recogniser.view tag]);
    NSLog(@"class = %@", [recogniser.view class]);
    
    if (![recogniser.view tag]==999)
        return;
    
    
    
    if(self.navigationController.navigationBar.isHidden)
    {
        [self.navigationController setNavigationBarHidden:NO
                                                 animated:YES];
        
        
        [self method_pan:self.imageView_currentImage.frame obj:self.imageView_currentImage];
        
        //        [self.tabBarController setTabBarHidden:NO
        //                                      animated:YES];
    }
    else
    {
        [self.navigationController setNavigationBarHidden:YES
                                                 animated:YES];
        
        [self method_pan:self.imageView_currentImage.frame obj:self.imageView_currentImage];
        
        //        [self.tabBarController setTabBarHidden:YES
        //                                      animated:YES];
    }
}


//#pragma mark - receiveTestNotification
//- (void) receiveTestNotification:(NSNotification *) notification
//{
//    // [notification name] should always be @"TestNotification"
//    // unless you use this method for observation of other notifications
//    // as well.
//    
//    
//    if ([[notification name] isEqualToString:@k_notification])
//    {
//        DLog(@"Successfully received the AR image notification!");
//        
//        NSDictionary* userInfo = notification.userInfo;
//        
//        if ([userInfo valueForKey:@k_ARImage])
//        {
//            DLog(@"%@", [userInfo valueForKey:@k_ARImage]);
//            self.string_imageURL = [userInfo valueForKey:@k_ARImage];
//            [self method_loadImageFromURL];
//        }
//        else
//        {
//            [[[UIAlertView alloc] initWithTitle:nil
//                                        message:@"There is something is wrong, please try again"
//                                       delegate:nil
//                              cancelButtonTitle:@"Ok"
//                              otherButtonTitles:nil, nil]
//             show];
//        }
//        
//    }
//}


@end