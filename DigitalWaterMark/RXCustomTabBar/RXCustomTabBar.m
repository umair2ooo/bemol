#import "RXCustomTabBar.h"

#import "ProductDetailViewController.h"
#import "ItemAvailabilityViewController.h"

@interface RXCustomTabBar()<UITabBarControllerDelegate>

@end

@implementation RXCustomTabBar

@synthesize btn1, btn2, btn3, btn4, btn5;

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.delegate = self;
    
    [self hideTabBar];
    [self addCustomElements];
    
    single = [Singleton retriveSingleton];
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)hideTabBar
{
	for(UIView *view in self.view.subviews)
	{
		if([view isKindOfClass:[UITabBar class]])
		{
			view.hidden = YES;
			break;
		}
	}
}


- (void)hideNewTabBar
{
    [self.btn1 setHidden:YES];
    [self.btn2 setHidden:YES];
    [self.btn3 setHidden:YES];
    [self.btn4 setHidden:YES];
    [self.btn5 setHidden:YES];
}

- (void)ShowNewTabBar 
{
    [self.btn1 setHidden:NO];
    [self.btn2 setHidden:NO];
    [self.btn3 setHidden:NO];
    [self.btn4 setHidden:NO];
    [self.btn5 setHidden:NO];
}

-(void)addCustomElements
{
    float float_xPosition = 0.0;
    float float_yPosition = self.view.frame.size.height-50;
    float float_buttonWidth = self.view.frame.size.width/5;
    float float_buttonHeight = 50.0;
    
    
	// Initialise our two images
	UIImage *btnImage = [UIImage imageNamed:@"NavBar_01.png"];
	UIImage *btnImageSelected = [UIImage imageNamed:@"NavBar_01_s.png"];
	
	self.btn1 = [UIButton buttonWithType:UIButtonTypeCustom]; //Setup the button
	btn1.frame = CGRectMake(float_xPosition,
                            float_yPosition,
                            float_buttonWidth,
                            float_buttonHeight); // Set the frame (size and position) of the button)
    
	[btn1 setBackgroundImage:btnImage forState:UIControlStateNormal]; // Set the image for the normal state of the button
	[btn1 setBackgroundImage:btnImageSelected forState:UIControlStateSelected]; // Set the image for the selected state of the button
	[btn1 setTag:0]; // Assign the button a "tag" so when our "click" event is called we know which button was pressed.
//	[btn1 setSelected:true]; // Set this button as selected (we will select the others to false as we only want Tab 1 to be selected initially
    
    float_xPosition = self.btn1.frame.origin.x + self.btn1.frame.size.width;
	
	// Now we repeat the process for the other buttons
	btnImage = [UIImage imageNamed:@"NavBar_02.png"];
	btnImageSelected = [UIImage imageNamed:@"NavBar_02_s.png"];
	self.btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(float_xPosition,
                            float_yPosition,
                            float_buttonWidth,
                            float_buttonHeight);
	[btn2 setBackgroundImage:btnImage forState:UIControlStateNormal];
	[btn2 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
	[btn2 setTag:1];
    
    
    float_xPosition = self.btn2.frame.origin.x + self.btn2.frame.size.width;
    
	//commit
	btnImage = [UIImage imageNamed:@"NavBar_03.png"];
	btnImageSelected = [UIImage imageNamed:@"NavBar_03_s.png"];
	self.btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(float_xPosition,
                            float_yPosition,
                            float_buttonWidth,
                            float_buttonHeight);
	[btn3 setBackgroundImage:btnImage forState:UIControlStateNormal];
	[btn3 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
    [btn3 setSelected:true];                      // Set this button as selected (we will select the others to false as we only want Tab 1 to be selected initially
	[btn3 setTag:2];
    
    
    float_xPosition = self.btn3.frame.origin.x + self.btn3.frame.size.width;
    
	
	btnImage = [UIImage imageNamed:@"NavBar_04.png"];
	btnImageSelected = [UIImage imageNamed:@"NavBar_04_s.png"];
	self.btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn4.frame = CGRectMake(float_xPosition,
                            float_yPosition,
                            float_buttonWidth,
                            float_buttonHeight);
	[btn4 setBackgroundImage:btnImage forState:UIControlStateNormal];
	[btn4 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
	[btn4 setTag:3];
    
    
    float_xPosition = self.btn4.frame.origin.x + self.btn4.frame.size.width;
    
    
    
    btnImage = [UIImage imageNamed:@"NavBar_05.png"];
    btnImageSelected = [UIImage imageNamed:@"NavBar_05_s.png"];
    self.btn5 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn5.frame = CGRectMake(float_xPosition,
                            float_yPosition,
                            float_buttonWidth,
                            float_buttonHeight);
    [btn5 setBackgroundImage:btnImage forState:UIControlStateNormal];
    [btn5 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
    [btn5 setTag:4];
    
    
    
    
	
	// Add my new buttons to the view
	[self.view addSubview:btn1];
	[self.view addSubview:btn2];
	[self.view addSubview:btn3];
	[self.view addSubview:btn4];
    [self.view addSubview:btn5];
    

	
	// Setup event handlers so that the buttonClicked method will respond to the touch up inside event.
	[btn1 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	[btn2 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	[btn3 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	[btn4 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn5 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self selectTab:2];
}

- (void)buttonClicked:(id)sender
{
    DLog(@"[sender tag]: %ld", (long)[sender tag]);
    
    DLog(@"single.productDetail: %@", single.productDetail);
    
    
    if (![single.array_cartObjects count] && [sender tag] == 0)
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Please add atleast one product to Shopping Cart"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
        
        return;
    }
    
    
    if (!single.productDetail && ([sender tag] == 0 || [sender tag] == 3))
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Scan the product first"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
        
        return;
    }


    if ([sender tag] == 1)
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                 message:@"Are you sure you want to call?"
                                delegate:self
                       cancelButtonTitle:@"Yes"
                       otherButtonTitles:@"No", nil]
      show];
        return;
    }
    
    
    if ([sender tag] == 2)
    {
        [self.delegate tabBarController:self didSelectViewController:self.selectedViewController];
    }
    
    [self selectTab:[sender tag]];
}


- (void)selectTab:(int)tabID
{
	switch(tabID)
	{
		case 0:
			[btn1 setSelected:true];
			[btn2 setSelected:false];
			[btn3 setSelected:false];
			[btn4 setSelected:false];
            [btn5 setSelected:false];
			break;
		case 1:
			[btn1 setSelected:false];
			[btn2 setSelected:true];
			[btn3 setSelected:false];
			[btn4 setSelected:false];
            [btn5 setSelected:false];
			break;
		case 2:
			[btn1 setSelected:false];
			[btn2 setSelected:false];
			[btn3 setSelected:true];
			[btn4 setSelected:false];
            [btn5 setSelected:false];
			break;
		case 3:
			[btn1 setSelected:false];
			[btn2 setSelected:false];
			[btn3 setSelected:false];
			[btn4 setSelected:true];
            [btn5 setSelected:false];
			break;
        case 4:
            [btn1 setSelected:false];
            [btn2 setSelected:false];
            [btn3 setSelected:false];
            [btn4 setSelected:false];
            [btn5 setSelected:true];
            break;
	}
	
	self.selectedIndex = tabID;
}



//- (void)dealloc
//{
//	[btn1 release];
//	[btn2 release];
//	[btn3 release];
//	[btn4 release];
//    [btn5 release];
//
//    [super dealloc];
//}

#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DLog(@"%ld", (long)buttonIndex);
    
    if (buttonIndex == 0)
    {
        NSString *number = @"+16303556292";
//        NSString *number = @"18005588724";
        
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",number]]];
    }
}


#pragma mark - tabbar controller delegate
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    id vc = [viewController isKindOfClass:[UINavigationController class]]? [(UINavigationController *) viewController topViewController]:nil;
    
    
    if (vc)
    {
        DLog(@"vc: %@, viewController: %@", [vc class], [viewController class]);
        if ([vc isKindOfClass:[ItemAvailabilityViewController class]] || [vc isKindOfClass:[ProductDetailViewController class]])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:k_notificationForTabBar
                                                                object:self
                                                              userInfo:nil];
        }
    }
}

@end